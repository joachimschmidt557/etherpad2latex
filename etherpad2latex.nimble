# Package

version       = "0.1.0"
author        = "joachimschmidt557"
description   = "Fetch and compile latex from etherpad"
license       = "MIT"
srcDir        = "src"
bin           = @["etherpad2latex"]



# Dependencies

requires "nim >= 0.20.0"
requires "jester >= 0.4.1"
requires "argparse >= 0.7.1"
