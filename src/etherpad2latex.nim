import asynchttpserver, asyncdispatch, httpclient
import os, osproc, strutils, parseopt

import jester
import argparse

proc getContent(pad:string, etherpadUrl:string): string =
  var client = newHTTPClient()
  result = client.getContent(etherpadUrl & "p/" & pad & "/export/txt")

when isMainModule:
  var p = newParser("etherpad2latex"):
    help("Compiles etherpad pads to pdf")
    option("-p", "--port", help="Port for the server", default="8080")
    option("-a", "--etherpad-address", help="The address of the etherpad", default="http://localhost:9001")
    option("-d", "--working-directory", help="The directory to use for saving and compiling", default=".")

  let opts = p.parse()

  router main:
    get "/@pad":
      let
        pad = @"pad"
        etherpadUrl = opts.etherpadAddress
        workingDir = opts.workingDirectory
        content = getContent(pad, etherpadUrl)

        texFile = workingDir / pad & ".tex"
        pdfFile = workingDir / pad & ".pdf"

      if fileExists(texFile) and readFile(texFile) == content and fileExists(pdfFile):
        # Use file from cache
        resp(Http200, readFile(pdfFile), contentType="application/pdf")
      else:
        writeFile(texFile, content)
        let (output, exitCode) = execCmdEx(quoteShellCommand(["pdflatex", "-halt-on-error", texFile]))
        if exitCode == 0:
          resp(Http200, readFile(pdfFile), contentType="application/pdf")
        else:
          resp output

  let s = newSettings(port=Port(opts.port.parseInt))
  var j = initJester(main, settings=s)
  j.serve()
