# etherpad2latex

`etherpad2latex` is a simple web server which listens to requests to build
a certain etherpad containing LaTex and serves the compiled PDFs.

I use it to create a collaborative space to work on LaTex documents of
any type.

## Dependencies

### Compile time

You'll need the [nim](https://nim-lang.org/) compiler toolchain including `nimble`,
the package manager for nim.

### Runtime

`etherpad2latex` requires `pdflatex`, typically obtained from
the [TexLive Latex distribution](https://tug.org/texlive/).

## Installation

### From source

```shell
git clone https://joachimschmidt557.xyz/gitea/joachimschmidt557/etherpad2latex
cd etherpad2latex
nimble install
```
